

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_TELE 12
#define MAX_ADDR 30

//一次性增加容量
#define INC_SZ 2
//初始容量
#define DEFAULT_SZ 3

typedef struct PeoInfo
{
	char name[MAX_NAME];
	int age;
	char sex[MAX_SEX];
	char tele[MAX_TELE];
	char addr[MAX_ADDR];
}PeoInfo;


//动态版本的通讯录
typedef struct Contact
{
	PeoInfo* data;//指向动态内存开辟的空间
	int sz;//通讯录有效信息个数
	int capacity;//通讯录当前容量
}Contact;


//初始化通讯录
void InitContact(Contact* pc);

//销毁通讯录
void DestroyContact(Contact* pc);

//增加指定联系人
void AddContact(Contact* pc);

//删除指定联系人
void DelContact(Contact* pc);

//查找指定联系人
void SearchContact(const Contact* pc);

//修改指定联系人
void ModifyContact(Contact* pc);

//展示通讯录
void ShowContact(const Contact* pc);

//排序通讯录
void SortContact(Contact* pc);