#define _CRT_SECURE_NO_WARNINGS 1

#include "contact.h"

int check_capacity(Contact* pc);

void LoadContact(Contact* pc)
{
	FILE* pf = fopen("Contact.txt", "rb");
	if (pf == NULL)
	{
		perror("InitContact::fopen");
		return;
	}
	PeoInfo tmp = { 0 };
	while ((fread(&tmp, sizeof(PeoInfo), 1, pf)))
	{
		int ret = check_capacity(pc);
		if (ret == 1)
		{
			pc->data[pc->sz] = tmp;
			pc->sz++;
		}
	}
	fclose(pf);
	pf = NULL;
}


void InitContact(Contact* pc)
{
	pc->data = (PeoInfo*)malloc(DEFAULT_CAPACITY * sizeof(PeoInfo));
	if (pc == NULL)
	{
		perror("malloc");
		return;
	}
	pc->sz = 0;
	pc->capacity = DEFAULT_CAPACITY;

	LoadContact(pc);
}

int check_capacity(Contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		PeoInfo* tmp = (PeoInfo*)realloc(pc->data, (pc->capacity + INC_CAPACITY) * sizeof(PeoInfo));
		if (tmp == NULL)
		{
			perror("增容失败，realloc");
			return 0;
		}
		else
		{
			pc->data = tmp;
			pc->capacity += INC_CAPACITY;
			printf("增容成功，当前容量:%d\n", pc->capacity);
			return 1;
		}
	}
	return 1;
}

void AddContact(Contact* pc)
{
	int ret = check_capacity(pc);
	if (ret == 1)
	{
		printf("请输入姓名:>");
		scanf("%s", pc->data[pc->sz].name);
		printf("请输入年龄:>");
		scanf("%d", &(pc->data[pc->sz].age));
		printf("请输入性别:>");
		scanf("%s", pc->data[pc->sz].sex);
		printf("请输入电话:>");
		scanf("%s", pc->data[pc->sz].tele);
		printf("请输入地址:>");
		scanf("%s", pc->data[pc->sz].addr);
		pc->sz++;
		printf("添加成功\n");
	}
}


void ShowContact(const Contact* pc)
{
	printf("%-10s %-4s %-5s %-12s %-20s\n", "姓名", "年龄", "性别", "电话", "地址");
	for (int i = 0; i < pc->sz; i++)
	{
		printf("%-10s %-4d %-5s %-12s %-20s\n", pc->data[i].name, pc->data[i].age
		, pc->data[i].sex, pc->data[i].tele, pc->data[i].addr);
	}
}


int FindByName(const Contact* pc, char* name)
{
	for (int i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
			return i;
	}
	return -1;
}

void DelContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入删除人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	for (int i = pos; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("删除成功\n");
}


void SearchContact(const Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return;
	}
	printf("%-10s %-4s %-5s %-12s %-20s\n", "姓名", "年龄", "性别", "电话", "地址");
	printf("%-10s %-4d %-5s %-12s %-20s\n", pc->data[pos].name, pc->data[pos].age
		, pc->data[pos].sex, pc->data[pos].tele, pc->data[pos].addr);
}

int cmp_by_name(const void* e1, const void* e2)
{
	return strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}

void SortContact(Contact* pc)
{
	qsort(pc->data, pc->sz, sizeof(PeoInfo), cmp_by_name);
	printf("排序成功\n");
}


void DistroyContact(Contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;
	printf("释放内存...\n");
}

void ModifyContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要修改人的名字:>");
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要修改人不存在\n");
		return;
	}
	printf("请输入姓名:>");
	scanf("%s", pc->data[pos].name);
	printf("请输入年龄:>");
	scanf("%d", &(pc->data[pos].age));
	printf("请输入性别:>");
	scanf("%s", pc->data[pos].sex);
	printf("请输入电话:>");
	scanf("%s", pc->data[pos].tele);
	printf("请输入地址:>");
	scanf("%s", pc->data[pos].addr);
	printf("修改成功\n");

}

void SaveContact(Contact* pc)
{
	FILE* pf = fopen("contact.txt", "wb");
	if (pf == NULL)
	{
		perror("SaveContact::fopen");
		return;
	}
	for (int i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data + i, sizeof(PeoInfo), 1, pf);
	}
	fclose(pf);
	pf = NULL;
	printf("保存数据中....\n");
}