#define _CRT_SECURE_NO_WARNINGS 1


//#include <stdio.h>
////定义一个学生变量
//struct Stu
//{
//	//成员变量
//	char name[20];
//	int age;
//	int num[20];
//} s4, s5, s6;//全局变量
//
//int main()
//{
//	struct Stu s1;//局部变量
//	struct Stu s2;
//	struct Stu s3;
//	return 0;
//}




//#include <stdio.h>
//
////把标签名省去，就是匿名结构体
//struct 
//{
//	int a;
//	char c;
//	double d;
//}s1;
//
//struct
//{
//	int a;
//	char c;
//	double d;
//}*ps;
//
//int main()
//{
//	ps = &s1;//err
//	return 0;
//}




//#include <stdio.h>
//
//struct Node
//{
//	int data;
//	struct Node* next;
//};
//
//int main()
//{
//	struct Node n1;
//	struct Node n2;
//	n1.next = &n2;
//	return 0;
//}


//#include <stdio.h>
//
//typedef struct
//{
//	int n;
//	char c;
//}S;//这里的S是匿名结构体的名字




//#include <stdio.h>
//
//typedef struct
//{
//	int data;
//	Node* next;
//}Node;
//
//int main()
//{
//
//	return 0;
//}




//#include <stdio.h>
//
//struct S
//{
//	int n;
//	char c;
//} s1;
//
//struct S s3;
//
//struct B
//{
//	float f;
//	struct S s;
//};
//
//int main()
//{
//	struct S s2 = { 100,'q' };
//	struct S s3 = { .c = 'p',.n = 2000 };
//
//	struct B b = { 3.14f,{50,'x'} };
//	printf("%f %d %c\n", b.f, b.s.n, b.s.c);
//	return 0;
//}





//#include <stdio.h>
//
//struct S
//{
//	char name[100];
//	int* ptr;
//};
//
//int main()
//{
//	struct S s = { "hahaha",NULL };
//	return 0;
//}



//#include <stdio.h>
//#include <stddef.h>
//struct S
//{
//	char c;
//	int n;
//};
//
//int main()
//{
//	printf("%d\n", offsetof(struct S, c));
//	printf("%d\n", offsetof(struct S, n));
//	return 0;
//}




//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};




//enum Sex
//{
//	//枚举的可能取值,默认从0开始，依次递增1
//	//枚举常量
//	MALE,//0
//	FEMALE=5,//5
//	SECRET//6
//};
//
//
//int main()
//{
//	enum Sex sex = MALE;
//	return 0;
//}
//




//enum OPTION
//{
//	EXIT,
//	RLAY,
//	ADD,
//	DEL
//};
//
//int main()
//{
//	enum OPTION op = EXIT;
//	return 0;
//}

