#define _CRT_SECURE_NO_WARNINGS 1


//#include <stdio.h>
////打印菱形
//int main()
//{
//	int line = 0;
//	scanf("%d", &line);
//	for (int i = 0; i < line; i++)
//	{
//		int j = 0;
//		//打印空格
//		for (j = 0; j < line - i - 1; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	for (int i = 0; i < line - 1; i++)
//	{
//		//打印空格
//		for (int j = 0; j <= i; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (int j = 0; j < 2 * (line - 1 - i) - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <math.h>
////打印自幂数0-100000
//int main()
//{
//	for (int i = 0; i <= 100000; i++)
//	{
//		int n = 0;
//		int tmp = i;
//		while (tmp)
//		{
//			n++;
//			tmp /= 10;
//		}
//		tmp = i;
//		int sum = 0;
//		while (tmp)
//		{
//			sum += pow(tmp % 10, n);
//			tmp /= 10;
//		}
//		if (sum == i)
//			printf("%d ", i);
//	}
//	return 0;
//}



////求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
////求a的前n项之和，不考虑溢出
//#include <stdio.h>
//int main()
//{
//	int a = 0;
//	int n = 0;
//	scanf("%d %d", &a, &n);
//	int sum = 0;
//	int ret = 0;
//	while (n)
//	{
//		ret = ret * 10 + a;
//		sum += ret;
//		n--;
//	}
//	printf("%d\n", sum);
//	return 0;
//}



////字符串逆序
//#include <stdio.h>
//#include <assert.h>
//#include <string.h>
//void reverse_str(char* str)
//{
//	assert(str);
//	char* left = str;
//	char* right = str + strlen(str) - 1;
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		right--;
//		left++;
//	}
//}
//int main()
//{
//	char arr[10001] = { 0 };
//	fgets(arr, 10000, stdin);//相当于gets，但比gets强stdin，从键盘输入，最大可以输入10000个字节
//	reverse_str(arr);
//	printf("%s\n", arr);
//	return 0;
//}


