#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//	int* p = (int*)malloc(20);
//	//可能会进行对NULL指针的解引用操作
//	for (int i = 0; i < 5; i++)
//	{
//		p[i] = i;
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

//int main()
//{
//	int* p = (int*)malloc(20);
//	//可能会进行对NULL指针的解引用操作
//	//所以malloc的返回值是要判断的
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	//越界访问
//	for (int i = 0; i < 10; i++)
//	{
//		p[i] = i;
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}




//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	//....
//	free(p);
//	p = NULL;
//	return 0;
//}




//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	for (int i = 0; i < 5; i++)
//	{
//		*p++ = i + 1;
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}





//
//int main()
//{
//	int* p = (int*)malloc(20);
//	if (p == NULL)
//	{
//		return 1;
//	}
//	//使用
//	//
//	free(p);
//	//释放
//	free(p);
//	p = NULL;
//	return 0;
//}


//void test()
//{
//	int* p = (int*)malloc(20);
//	if (p != NULL)
//	{
//		for (int i = 0; i < 5; i++)
//		{
//			p[i] = i;
//		}
//	}
//	//不释放，造成内存泄漏
//}
//
//int main()
//{
//	test();
//
//	return 0;
//}


