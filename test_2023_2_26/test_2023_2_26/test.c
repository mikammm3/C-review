#define _CRT_SECURE_NO_WARNINGS 1


//#include<stdio.h>
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	//函数指针
//	int (*p)(int, int) = Add;
//	//函数指针数组
//	int (*pArr[4])(int, int) = { Add,Sub };
//	//指向函数指针数组的指针
//	int (*(*ppArr)[4])(int, int) = &pArr;
//	return 0;
//}







//#include<stdio.h>
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//void menu()
//{
//	printf("*****************************\n");
//	printf("******   1.add    2.sub *****\n");
//	printf("******   3.mul    4.div *****\n");
//	printf("******   0.exit         *****\n");
//	printf("*****************************\n");
//}
//
////回调函数解决代码冗余问题
//void Calc(int (*pf)(int, int))
//{
//    int x = 0;
//	int y = 0;
//	printf("请输入两个操作数:>");
//	scanf("%d %d", &x, &y);
//	int ret = pf(x, y);
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	void* p = &a;//void* - 是无具体类型的指针，所以它可以接受任意类型的地址
//	*p;//err,void*的指针不能进行解引用操作，也不能加减整数
//	return 0;
//}


//#include<stdio.h>
//#include<stdlib.h>
//
//int cmp_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void print_arr(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void test1()
//{
//	//使用qsort排序整型数组,qsort默认排的是升序
//	int arr[] = { 2,1,3,6,4,5,7,9,8,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_arr(arr,sz);
//}
//
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//int cmp_stu_by_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//int cmp_stu_by_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//
//void test2()
//{
//	//使用qsort 排序结构体数据
//	struct Stu s[] = { {"zhangsan",30},{"lisi",25},{"wangwu",50} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	//qsort(s, sz, sizeof(s[0]), cmp_stu_by_age);
//	qsort(s, sz, sizeof(s[0]), cmp_stu_by_name);
//}
//
//void Swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_sort(void* base, size_t num, size_t width, int (*cmp)(const void* p1, const void* p2))
//{
//	size_t i = 0;
//	//确定冒泡排序的趟数
//	for (i = 0; i < num - 1; i++)
//	{
//		int flag = 1;
//		size_t j = 0;
//		//一趟冒泡排序
//		for (j = 0; j < num - 1 - i; j++)
//		{
//			//两个相邻元素的比较
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				//交换
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//			break;
//	}
//}
//
//void test3()
//{
//	//模拟实现通用的冒泡排序
//	int arr[] = { 2,1,3,6,4,5,7,9,8,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_arr(arr, sz);
//}
//
//int main()
//{
//	//test1();
//	//test2();
//	test3();
//	return 0;
//}