#define _CRT_SECURE_NO_WARNINGS 1



//#include <stdio.h>
//
//int main()
//{
//	//一维数组
//	//数组名表示的是数组首元素的地址，但是有两个例外
//	//sizeof(数组名)，数组名表示的是整个数组，求的是整个数组的大小，单位是字节
//	//&数组名，数组名表示整个数组，取出的是整个数组的大小。
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));//16 - 数组名单独放在sizeof内部，表示整个数组
//	printf("%d\n", sizeof(a + 0));// 4/8 数组名没有单独放在里面，表示的是首元素地址
//	printf("%d\n", sizeof(*a));//4 数组名表示首元素地址，解引用拿到首元素
//	printf("%d\n", sizeof(a + 1));// 4/8 首元素地址+1，地址+1还是地址，是地址就是4/8个字节
//	printf("%d\n", sizeof(a[1]));//4 第二个元素的大小
//	printf("%d\n", sizeof(&a));// 4/8 &a表示的是整个数组的地址，地址就是4/8个字节
//	printf("%d\n", sizeof(*&a));//16  *&的作用抵消，所以相当于a单独放在sizeof内部，表示整个数组，计算的就是整个数组的大小
//	printf("%d\n", sizeof(&a + 1));// 4/8  取出整个数组地址，加1跳过整个数组，但地址就是地址，就是4/8字节
//	printf("%d\n", sizeof(&a[0]));// 4/8  取出首元素的地址，就是4/8字节
//	printf("%d\n", sizeof(&a[0] + 1));//4/8  取出首元素地址再+1，就是第二个元素的地址，就是4/8字节
//
//	return 0;
//}




//#include <stdio.h>
//int main()
//{
//	//字符数组
//	char arr[] = { 'a','b','c','d','e','f' };
//
//	printf("%d\n", strlen(arr));//随机值  strlen是用来求字符串的长度，遇到'\0'才停止，而数组中没有\0
//	printf("%d\n", strlen(arr + 0));//随机值    同上
//	printf("%d\n", strlen(*arr));//err
//	//strlen的参数是const char*，是指针，把a传给了strlen，就意味着a的ASCII码值被当作地址，进行了非法访问
//	printf("%d\n", strlen(arr[1]));//err  同上，把b传给了strlen
//	printf("%d\n", strlen(&arr));//随机值 &arr是从首元素地址开始的
//	printf("%d\n", strlen(&arr + 1));//随机值-6  &arr表示整个数组地址，+1跳过整个数组，所以跟上面的随机值相差6
//	printf("%d\n", strlen(&arr[0] + 1));//随机值-1 取出首元素地址，+1就是第二个元素地址，跟上面随机值比差了1
//
//
//	//printf("%d\n", sizeof(arr));//6 数组名单独放在sizeof内部，表示整个数组。
//	//printf("%d\n", sizeof(arr + 0));// 4/8 数组名表示首元素地址，地址就是4/8个字节
//	//printf("%d\n", sizeof(*arr));//1  数组名表示首元素地址，解引用后得到的是首元素
//	//printf("%d\n", sizeof(arr[1]));//1
//	//printf("%d\n", sizeof(&arr));// 4/8  &数组名表示整个数组，取出整个数组的地址，是4/8个字节
//	//printf("%d\n", sizeof(&arr + 1));//4/8  同上
//	//printf("%d\n", sizeof(&arr[0] + 1));// 4/8  取出首元素的地址，+1跳过一个整型，是第二个元素地址
//
//	return 0;
//}




//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	//char arr[] = "abcdef";
//	//printf("%d\n", sizeof(arr));//7 数组里面存放了a b c d e f \0
//	//printf("%d\n", sizeof(arr + 0));//4/8  arr是首元素地址
//	//printf("%d\n", sizeof(*arr));//1  首元素地址解引用得到首元素
//	//printf("%d\n", sizeof(arr[1]));//1
//	//printf("%d\n", sizeof(&arr));//4/8  &arr得到整个数组的地址，是地址就是4/8
//	//printf("%d\n", sizeof(&arr + 1));//4/8  跳过整个数组的地址
//	//printf("%d\n", sizeof(&arr[0] + 1));//4/8  第二个元素的地址
//
//	char arr[] = "abcdef";
//	printf("%d\n", strlen(arr));//6 找'\0'
//	printf("%d\n", strlen(arr + 0));//6
//	printf("%d\n", strlen(*arr));//err  把a传给strlen
//	printf("%d\n", strlen(arr[1]));//err  把b传给strlen
//	printf("%d\n", strlen(&arr));//6
//	printf("%d\n", strlen(&arr + 1));//随机值，从 跳过整个数组的地址 开始计算
//	printf("%d\n", strlen(&arr[0] + 1));//5  从第二个元素的地址  开始计算
//
//	return 0;
//}




//
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	//char* p = "abcdef";
//	//printf("%d\n", sizeof(p));//4/8  char*
//	//printf("%d\n", sizeof(p + 1));//4/8  地址
//	//printf("%d\n", sizeof(*p));//1   解引用拿到首元素
//	//printf("%d\n", sizeof(p[0]));//1  
//	//printf("%d\n", sizeof(&p));//4/8   地址就是4/8
//	//printf("%d\n", sizeof(&p + 1));//4/8   同理
//	//printf("%d\n", sizeof(&p[0] + 1));//4/8   第二个元素的地址
//
//
//
//	char* p = "abcdef";
//	printf("%d\n", strlen(p));//6 找\0
//	printf("%d\n", strlen(p + 1));//5 首元素地址+1就是第二个元素地址
//	printf("%d\n", strlen(*p));//err  把a传给strlen
//	printf("%d\n", strlen(p[0]));//err  同上
//	printf("%d\n", strlen(&p));//随机值   p是一个指针变量，则从指针变量的地址开始计算
//	printf("%d\n", strlen(&p + 1));//随机值   则从 指针变量跳过一个对应类型的地址 开始计算
//	printf("%d\n", strlen(&p[0] + 1));//5  第二个元素地址
//
//	return 0;
//}




//#include <stdio.h>
//
//int main()
//{
//
//	//二维数组
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a));//48    单独，表示整个数组
//	printf("%d\n", sizeof(a[0][0]));//4
//	printf("%d\n", sizeof(a[0]));//16    a[0]是第一行的数组名，单独，表示整个数组的大小
//	printf("%d\n", sizeof(a[0] + 1));//4/8  a[0]是第一行数组名，不单独，表示首元素地址，+1表示a[0][1]的地址
//	printf("%d\n", sizeof(*(a[0] + 1)));//4    表示a[0][1]
//	printf("%d\n", sizeof(a + 1));//4/8  表示首元素地址，a[0]的地址-->&a[0],+1就是a[1]的地址
//	printf("%d\n", sizeof(*(a + 1)));//16  计算a[1]总大小
//	printf("%d\n", sizeof(&a[0] + 1));//4/8  相当于a[1]的地址
//	printf("%d\n", sizeof(*(&a[0] + 1)));//16  相当于a[1],求a[1]总大小
//	printf("%d\n", sizeof(*a));//16   首元素地址 &a[0]再解引用相当于a[1]
//	printf("%d\n", sizeof(a[3]));//16    没有真正计算，编译器推理得出
//
//	return 0;
//}



