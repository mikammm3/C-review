#define _CRT_SECURE_NO_WARNINGS 1


//#include <stdio.h>
//
//int main()
//{
//	int a = 10;
//	printf("%d-------%s %s %s line=%d\n", a, __FILE__, __DATE__, __TIME__, __LINE__);
//	return 0;
//}




//#include <stdio.h>
//
//int main()
//{
//	printf("%s\n", __FUNCTION__);
//	printf("%s\n", __FUNCDNAME__);
//	return 0;
//}



//#include <stdio.h>
////正确写法
//#define SQUARE(x) ((x)*(x))
//
//int main()
//{
//	printf("%d\n", SQUARE(1 + 7));
//	return 0;
//}


//#include <stdio.h>
//
//#define print_format(num,format) \
//        printf("the value of "#num" is "format,num)
//int main()
//{
//	int a = 10;
//	print_format(a, "%d\n");
//
//	return 0;
//}

#include <stdio.h>


//#define MAX(a, b) ( (a) > (b) ? (a) : (b) )

//int main()
//{
//	int x = 5;
//	int y = 8;
//	int z = MAX(x++, y++);
//	//int z = ((x++) > (y++) ? (x++) :(y++));
//	//这时x猛++，y也猛++，值就会发生改变
//	printf("x=%d y=%d z=%d\n", x, y, z); //6 10 9
//
//	return 0;
//}



